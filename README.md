# README

# Caring For Carers (C4C)

**C4C** is an R package developed to support the mental health and wellbeing of mental health and psychosocial support (MHPSS) practitioners working with displaced communities. 

The package provides tools to randomise and counterbalance for longitudinal participant recruitment, ensuring unbiased representation of days-of-the-week or time-of-the-day across an arbitrarily-sized set of participants. The package also provides tools to generate error-correcting unique identifiers based on string distances. In principle, only a few correct consecutive characters (alphanumerics, symbols etc.) are required to recover full identifiers ~ particularly important when English-language keyboards are being used by non-English-speaking participants, or, in contexts where internet connections are frequently interrupted; such as in conflict-affected settings. These tools together offer novel but important functionality for researchers in the fields of psychiatric epidemiology and applied statistics.

The package was developed as part of a mixed-methods research study aiming to determine the impact of an online video conferencing, peer-supervision programme on the wellbeing of local MHPSS practitioners working with Syrian and Rohingya displaced communities in Syria, Turkey, and Bangladesh. More information about the project can be found [here](https://www.elrha.org/project/caring-for-carers-a-psychosocial-supervision-intervention/).

To access the documentation on the full implementation of the **Caring For Carers** project, please see: Wells, R, et al., (*Forthcoming*) and related Supplementary Materials.

- For simulation studies on the application and evaluation of the statistical approach taken for randomisation and counterbalancing, please see: Klein, L., et al., (*In Preparation*).
- For simulation studies on the use of error-correcting IDs, including fault-tolernace metrics, please see: Klein, L., et al., (*In Preparation*).

## Installation

You can install the package using the following commands:

```R
# Install devtools if not already installed
if (!require(devtools)) {
  install.packages("devtools")
}

# Install C4C from GitLab
devtools::install_git("https://gitlab.com/louisdesu/elrha-c4c.git")
```

## Usage

The **C4C** package currently contains two main functions: `ranid()` and `counterbalance_across()`. 

### `ranid()`

This function generates error-correcting IDs based on the specified parameters. It uses the `ids` package to generate cryptographic random strings and applies string distance calculations for error correction. Only several consecutively correct characters are typically required to recover an ID string from errors, or distinguish an ID string from similar alternative strings.

#### Parameters:

- `target_n`: The number of IDs to be generated. It must be less than or equal to `starting_size`.
- `max_symbols`: The maximum number of symbol replacements (like '#', '&', '@', '*', '!', '%') to be included in the IDs for added complexity.
- `starting_size`: The initial pool size for the random ID generation. The larger the `starting_size`, the more distinct IDs can be generated.
- `dist_method`: The string distance calculation method to be used. Default is "lv" (Levenshtein), but "hamming" can also be used as per the `stringdist` **R** package.
- `dist_value`: The minimum string distance value. IDs with a string distance less than this value are discarded, ensuring error detection and correction capability.
- `str_length`: The length of the string IDs to be generated. Must be greater than or equal to 8.
- `rows_column`: A boolean to decide whether to include a 'rowid' column in the output.
- `set_seed`: Seed value for random number generation for reproducibility. If `NULL`, a random seed is chosen.
- `details`: A boolean to decide whether to include detailed statistics about the string distances ('median', 'sd', and 'range') in the output.

#### Example Usage:

```R
# Generate 50 error-correcting codes with 1 or fewer symbols per string
ranid(target_n = 50, max_symbols = 1, starting_size = 5000, details = TRUE)
```

```R
tibble [5,000 × 5] (S3: tbl_df/tbl/data.frame)
 $ rowid : int [1:50] 1 2 3 4 5 6 7 8 9 10 ...
 $ ranids: chr [1:50] "cdda932e4ec2a3b6e212" "464aca3c028ac&3fc82d" "a450fb133404f847e003" "1cf000!1863430468615" ...
 $ median: num [1:50] 18 17.5 18 18.5 18 18 18 17 18 18 ...
 $ sd    : num [1:50] 1.38 1.08 1.28 1.27 1.05 ...
 $ range : chr [1:50] "15-19" "16-20" "15-20" "16-20" ...
```

#### Usage notes:
- setting `starting_size` `1E5` may lead to memory collapse and abort session
- please choose a value for `str_length` >= `8`
- ensure `target_n` <= `starting_size`
- `target_n` required

### `counterbalance_across()`

The `counterbalance_across()` function, a part of the `c4c` package, addresses a common challenge in empirical epidemiological and psychological research: Achieving a uniform distribution of a parameter, such as days of the week, when randomising this parameter for each participant or data entry.

The function works by randomly assigning days of the week to the specified variable in the data frame. The variance of the assigned days is calculated at each iteration. If the variance approximates the set threshold, the solution is accepted. If not, the solution is rejected, and a new candidate solution is generated. This process repeats until an appropriate distribution of days is achieved.

This approach is flexible and can be extended to multiple input parameters using grouping variables (for e.g., `dplyr::group_by(x, y)`).

#### Parameters:

- `data`: A grouped data frame or tibble with an ID column (for e.g., `tibble::rowid_to_column()`) and a `list_name` column.
- `list_name`: The name of the variable in `data` that contains the features to be randomised.
- `threshold`: The threshold value for standard deviation of the variable represented in `list_name`. The function continues to generate candidate randomisations until the variance approximates this threshold (default is 1.0).
- `print_runs`: A boolean indicating whether to print the variance at each iteration (default is FALSE).

#### Example Usage:

First, you have to prepare an appropriate dataframe with the features to be randomised and counterbalanced:

```R
# Required library
library(dplyr)

# Define the days of the week
days_of_week <- c("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday")

# Create a data frame
df <- data.frame(rowid = 1:10)

# Generate random days for each row
df$days <- sapply(1:nrow(df), function(x) {
  sample(days_of_week, sample(2:7, 1), replace = FALSE)
})
```

```R
'data.frame':	10 obs. of  2 variables:
 $ rowid: int  1 2 3 4 5 6 7 8 9 10
 $ days :List of 10
  ..$ : chr  "Tuesday" "Monday"
  ..$ : chr  "Thursday" "Sunday" "Tuesday" "Wednesday" ...
  ..$ : chr  "Sunday" "Friday" "Thursday"
  ..$ : chr  "Tuesday" "Thursday" "Monday" "Wednesday"
  ..$ : chr  "Wednesday" "Sunday" "Thursday" "Friday" ...
  ..$ : chr  "Tuesday" "Wednesday" "Saturday" "Friday" ...
  ..$ : chr  "Saturday" "Friday"
  ..$ : chr  "Wednesday" "Saturday"
  ..$ : chr  "Saturday" "Tuesday"
  ..$ : chr  "Wednesday" "Friday" "Sunday"
```

Then you can apply `counterbalance_across()`, note again the call expects an explicit grouping variable:

```R
counterbalance_across(data = group_by(df,rowid), list_name = days)
```

```R
tibble [10 × 2] (S3: tbl_df/tbl/data.frame)
 $ rowid: int [1:10] 1 2 3 4 5 6 7 8 9 10
 $ days : chr [1:10] "Tuesday" "Tuesday" "Sunday" "Monday" ...
```

## Dependencies

C4C relies on a number of R packages including `ids`, `dplyr`, `tidyr`, `stringdist`, and `tibble`. Additionally, `counterbalance_across()` uses the `%->%` output pipe from the `louisdesu` library.

## Contributing

Contributions welcome. Please follow standard coding conventions and make sure to thoroughly test your code before submitting. 

## License

This project is licensed under the terms of the MIT license.